import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeroeModel } from '../models/heroe.model';
import { map, delay } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private url = 'https://controlllegada.firebaseio.com';

  constructor(private http: HttpClient,
              private firestore: AngularFirestore) { }         


  crearEmpleado(heroe: HeroeModel) {
    return this.http.post(`${ this.url }/Empleados.json`, heroe)
      .pipe(
        map((resp: any) => {
          heroe.id = resp.name;
          return heroe;
        })
      );
  }

  actualizarEmpleado(heroe: HeroeModel) {
    const empleadoTemp = {
      ...heroe
    };
    delete empleadoTemp.id;
    return this.http.put(`${this.url}/Empleados/${heroe.id}.json`, empleadoTemp);
  }

  borrarEmpleado(id: string){
    return this.http.delete(`${ this.url }/Empleados/${ id }.json`);
  }

  getEmpleado(id: string){
    return this.http.get(`${ this.url }/Empleados/${ id }.json`);
  }

  getEmpleados(){
    return this.http.get(`${ this.url }/Empleados.json`)
    .pipe(
      map( this.crearArreglo ),
      delay(0)   //tiempo en que se demora el cargando
    );
  }

  private crearArreglo(heroesObj: object){
    const empleados: HeroeModel[] = [];
    //console.log(heroesObj);
    if ( heroesObj === null ) { return []; }
    Object.keys( heroesObj ).forEach( key => {
      const heroe: HeroeModel = heroesObj[key];
      heroe.id = key;
      empleados.push( heroe );
    });
    return empleados;
  }

}
