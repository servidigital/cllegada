import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeroeComponent } from './pages/heroe/heroe.component';
import { EmpleadosComponent } from './pages/empleados/empleados.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { XsegundoService } from './services/xsegundo.service';

//angular fire
import { AngularFireModule } from  '@angular/fire';
import { AngularFirestoreModule } from  '@angular/fire/firestore';

//environment
import { environment } from '../environments/environment';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HeroeComponent,
    EmpleadosComponent, 
    InicioComponent, LoginComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase, 'my-app'),
    AngularFirestoreModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    HttpClientModule
  ],
  providers: [XsegundoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
