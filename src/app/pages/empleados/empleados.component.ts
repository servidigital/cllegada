import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { HeroeModel } from 'src/app/models/heroe.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {

  empleados: HeroeModel[] = [];
  cargando = false;

  constructor(private heroesService: HeroesService) {
   }

  ngOnInit() {
    this.cargando = true;
    this.heroesService.getEmpleados()
      .subscribe(resp => {
        //console.log(resp);
        this.empleados = resp;
        this.cargando = false;
        //console.log(this.empleados);
      });
  }

  eliminarEmpleado(heroe: HeroeModel, i: number) {
    Swal.fire({
      title: '¿Está seguro?',
      text: `Está seguro que desea borrar a ${heroe.nombre}`,
      type: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then(resp => {
      if (resp.value) {
        this.empleados.splice(i, 1);
        this.heroesService.borrarEmpleado(heroe.id).subscribe();
      }
    });
  }

  historialEmpleado(){
    
  }

}
