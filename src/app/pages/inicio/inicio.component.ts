import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { XsegundoService, valorReloj } from 'src/app/services/xsegundo.service';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  datos$: Observable<valorReloj>;
  year: string;
  mes: string;
  dias: string;
  hora: number;
  hora24: string;
  minutos: string;
  dia: string;
  fecha: string;
  ampm: string;
  segundos: string;

  h:any;
  vr:any;

  d:any;
  constructor(private segundo: XsegundoService) { }

  ngOnInit() {
    this.datos$=this.segundo.getInfoReloj();
    this.datos$.subscribe(x => {
      this.year = x.year;
      this.mes = x.mes;
      this.dias = x.dias;
      this.hora = x.hora;
      this.hora24 = x.hora24;
      this.minutos = x.minutos;
      this.dia = x.diadesemana;
      this.fecha = x.diaymes;
      this.ampm = x.ampm;
      this.segundos = x.segundo
    });

  }

  llegada(){

    console.log('Llegada '+this.year+'-'+this.mes+'-'+this.dias+' '+this.hora24+':'+this.minutos+':'+this.segundos);

  }

  salida(){
    console.log('Salida '+this.year+'-'+this.mes+'-'+this.dias+' '+this.hora24+':'+this.minutos+':'+this.segundos);
  }

}
